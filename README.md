# **Animal Crossing Market · Back**

[![php](https://img.shields.io/static/v1?label=PHP&message=v7.4&color=777BB4&style=flat-square&logo=php&logoColor=white)](https://www.php.net/)
[![laravel](https://img.shields.io/static/v1?label=Laravel&message=v7.19&color=FF2D20&style=flat-square&logo=laravel&logoColor=white)](https://laravel.com/)

[![node.js](https://img.shields.io/static/v1?label=Node.js&message=v12.15&color=339933&style=flat-square&logo=node.js&logoColor=white)](https://nodejs.org/en/)
[![yarn](https://img.shields.io/static/v1?label=Yarn&message=v1.22&color=2C8EBB&style=flat-square&logo=yarn&logoColor=white)](https://classic.yarnpkg.com/lang/en/)

[![nginx](https://img.shields.io/static/v1?label=NGINX&message=v1.14&color=269539&style=flat-square&logo=nginx&logoColor=white)](https://www.nginx.com/)
[![swagger](https://img.shields.io/static/v1?label=Swagger&message=v3&color=85EA2D&style=flat-square&logo=swagger&logoColor=white)](https://swagger.io/)

## **I. Project setup**

Create a new database, you can call it `ac-market` like in the `.env.ple`, with phpMyAdmin or directly with MySQL CLI. Create `.env` file and replace database variables.

```bash
cp .env.example .env
```

```yml
DB_DATABASE=ac-market
DB_USERNAME=root
DB_PASSWORD=
```

Setup the project

```
composer install
php artisan key:generate
php artisan migrate:fresh --seed
yarn
yarn dev
```

Now you can access to the app with `php artisan serve` or you can setup NGINX VHost. You can use Apache to create VHost if you want, NGINX is just better for this.

### **NGINX VHOST**

At the root of the repository, execute these commands to be sure to allow NGINX to have rights on these directories.

```bash
sudo chmod -R ug+rwx storage bootstrap/cache
sudo chown -R $USER:www-data vendor storage
```

Create new config into `/etc/nginx/sites-available`, call it `ac-market` for example.

Make sure to replace `/var/www/ac-market-back` with another path if you clone the repository in another path.

```nginx
server {
    listen 80;
    root /var/www/ac-market-back/public;
    index index.php index.html index.htm index.nginx-debian.html;
    server_name ac-market.localhost;
    
    error_log /var/log/nginx/ac-market.log warn;

    location / {
        try_files $uri $uri/ /index.php$is_args$args;
    }
    
    location ~ \.php$ {
        include snippets/fastcgi-php.conf;
        fastcgi_pass unix:/var/run/php/php7.4-fpm.sock;
    }

    location ~ /\.ht {
        deny all;
    }
}
```

```bash
sudo ln -s /etc/nginx/sites-available/ac-market /etc/nginx/sites-enabled
sudo nginx -t
sudo service nginx reload
```

The application will be available on [**http://ac-market.localhost**](http://ac-market.localhost)

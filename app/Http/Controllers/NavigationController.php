<?php

namespace App\Http\Controllers;

class NavigationController extends Controller
{
    public function welcome()
    {
        $websites = [
            'acnhapi-com' => [
                'label'       => 'acnhapi.com',
                'link'        => 'http://acnhapi.com/',
                'description' => '',
            ],
            'nookazon-com' => [
                'label'       => 'nookazon.com',
                'link'        => 'https://nookazon.com/',
                'description' => '',
            ],
            'nookplaza-net' => [
                'label'       => 'nookplaza.net',
                'link'        => 'https://nookplaza.net/',
                'description' => '',
            ],
            'data-sheet' => [
                'label'       => 'Data Sheet',
                'link'        => 'https://docs.google.com/spreadsheets/d/13d_LAJPlxMa_DubPTuirkIV4DERBMXbrWQsmSh8ReK4/edit#gid=1492267482',
                'description' => '',
            ],
            'acnh-tnrd-net' => [
                'label'       => 'acnh.tnrd.net',
                'link'        => 'https://acnh.tnrd.net/index.html?urls.primaryName=V3',
                'description' => '',
            ],
            'norviah-animal-crossing' => [
                'label'       => 'Norviah/animal-crossing',
                'link'        => 'https://github.com/Norviah/animal-crossing',
                'description' => '',
            ],
        ];

        return view('welcome', compact('websites'));
    }
}

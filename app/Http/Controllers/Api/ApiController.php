<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use App\Http\Controllers\Controller;

class ApiController extends Controller
{
    /**
     * @OA\Info(
     *     version="1.0.0",
     *     title="AC Market API",
     *     description="AC Market Swagger Front API"
     * ),
     * @OA\Tag(
     *     name="global",
     *     description="Requêtes globale"
     * )
     */
    /**
     * @OA\Get(
     *     path="/users",
     *     tags={"global"},
     *     summary="Users list",
     *     description="Users",
     *     @OA\Response(
     *         response=200,
     *         description="Successful operation"
     *     )
     * )
     */
    public function getUsers()
    {
        $users = User::all();

        return $users;
    }
}

<?php

use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\Hash;
use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'name' => 'Ewilan',
                'email' => 'ewilan@dotslashplay.it',
                'password' => Hash::make('password')
            ]
        ];

        foreach ($users as $key => $user) {
            User::create($user);
        }
    }
}
